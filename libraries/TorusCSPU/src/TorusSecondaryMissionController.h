/*
 * TorusSecondaryMissionController.h
 */

#pragma once
#include "TorusRecord.h"
#include "TorusServoWinch.h"
#include "elapsedMillis.h"
#include "TorusConfig.h"

/** The class in charge of controlling the Torus flight, ie.:
 * 		- Computing the reference altitude, from which relative
 * 		  altitude is counted.
 * 		- Check flight safety conditions
 * 		- If in nominal descent phase, define the flight phase
 * 		  the can is currently in, depending on the phases
 * 		  defined in TorusConfig.h
 * 		- Whatever the conclusion on the safety conditions and
 * 		  flight phase, set the appropriate target for the
 * 		  servo winch.
 * 	It also fills the record with all secondary mission-related
 * 	information:
 * 		- Reference altitude
 * 		- ControllerInfo
 * 		- Flight phase
 * 		- servo-winch current target and current position.
 *  All settings are defined in TorusConfig.h, and the class
 *  ensures that flight phase does not change more than every
 *  TorusMinDelayBetweenPhaseChange msec, and servo target are
 *  not change more often than every TorusMinDelayBetweenTargetUpdate
 *  msec.
 *
 *  @usage
 *  @code
 *  	TorusSecondaryMissionController ctrl;
 *  	// once:
 *  	ctrl.begin(theServo);
 *  	// whenever a record is acquired:
 *  	// Preferably call in
 *  	// TorusAcquisitionProcess::acquireSecondaryMissionData()
 *  	ctrl.run(record);
 *  @endcode
 */
class TorusSecondaryMissionController {
public:
	/** Constructor */
	TorusSecondaryMissionController();

	/** Initialize the controller before use
	 * @param theServo  A reference to the ServoWinch object to use
	 * @return true if initialization is successful, false otherwise.
	 */
	bool begin(TorusServoWinch& theServo);

	/** The main entry point for the controller. It should be called
	 *  from the process acquireSecondaryMissionData() method.
	 *  @param record The Torus record, already complete with all primary
	 *  mission data.
	 */
	void run(TorusRecord& record);
};

