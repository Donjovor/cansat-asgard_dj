/*
 * TorusAcquisitionProcess.h
 */

#pragma once
#include "CansatAcquisitionProcess.h"
#include "TorusHW_Scanner.h"
#include "TorusRecord.h"

class TorusAcquisitionProcess: public CansatAcquisitionProcess {
public:
	TorusAcquisitionProcess() {};
	virtual ~TorusAcquisitionProcess() {}	;

	/** Return a pointer to an instance of a TorusHW_Scanner. It can be overridden by subclasses to provide
	 *  an instance of an appropriate subclass of HardwareScanner.
	 *  The object is NOT initialised: its init() method will be called during the AcquisitionProcess'
	 *  initialization phase.
	 *  The object is dynamically allocated. Ownership of the object is transferred and it will be
	 *  released when the CansatAcquisitionProcess is released.
	 */
	virtual TorusHW_Scanner* getNewHardwareScanner() {
		return new TorusHW_Scanner();
	}
	;

	/** Obtain the TorusHW_Scanner initialized by the process in method initHW_Scanner()
	 */
	virtual TorusHW_Scanner* getHardwareScanner() {
		return (TorusHW_Scanner*) CansatAcquisitionProcess::getHardwareScanner();
	}
	;

    /** Return a pointer to an instance of a TorusRecord. It is overridden to provide
       *  an instance of an appropriate subclass of CansatRecord.
       *  The record should be dynamically allocated. Ownership of the object is transferred and it will be
       *  released when the CansatAcquisitionProcess is released.
       */
      virtual TorusRecord* getNewCansatRecord() { return new TorusRecord;};

};

