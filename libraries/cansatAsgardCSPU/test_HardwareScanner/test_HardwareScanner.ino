/*
   Test program for HardwareScanner and CansatHW_Scanner classes

   Wiring:
    Be sure to use 5 or 10k pull-ups on SDA and SCL. Note that on Uno board, with no slave and no pull-ups the absence of slaves
    is correctly detected, while on the Feather M0 Express, pull-ups are required to avoid erratic readings and program hanging
    during bus scan.

   Note: utility sketches HardwareScanner and CansatHW_Scanner (in utils folder) can also be used to check with various hardware 
         configurations
*/
#include "CansatHW_Scanner.h"
#include "HW_Subclass.h"
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatConfig.h"

void checkAvailabilityOfSerialPorts(const HardwareScanner& hw) {
  Serial.println("");
  Serial.print("Checking availability of serial ports: ");
  for (int i = 0; i < 4; i++) {
    Serial.print(i);
    Serial.print(F(":"));
    Serial.print(hw.isSerialPortAvailable(i));
    Serial.print(F(" "));
  }
}

void testHW_ScannerWithoutRF_Port() {
  Serial.println("*** Testing HardwareScanner without RF nor GPS port....");
  Serial.println("Full diagnostic:");
  CansatHW_Scanner hw;
  hw.init(4, 10, 100,0, 0);
  hw.printFullDiagnostic(Serial);
  checkAvailabilityOfSerialPorts(hw);
  Serial.println("");

  DASSERT(hw.getNumExternalEEPROM() == 0);
  DASSERT(hw.getExternalEEPROM_I2C_Address(25) == 0);
#ifdef RF_ACTIVATE_API_MODE
  DASSERT(hw.getRF_XBee() == NULL);
#else
  DASSERT(hw.getRF_SerialObject() == NULL);
#endif
  DASSERT(hw.getLED_PinNbr(LED_Type::Transmission) == pin_TransmissionLED);
  DASSERT(hw.getLED_PinNbr(LED_Type::Storage) == pin_StorageLED);
  DASSERT(hw.getLED_PinNbr(LED_Type::Acquisition) == pin_AcquisitionLED);
  DASSERT(hw.getLED_PinNbr(LED_Type::Heartbeat) == pin_HeartbeatLED);
  DASSERT(hw.getLED_PinNbr(LED_Type::Init) == pin_InitializationLED);
  Serial.println("");
}

void testHW_ScannerWithRF_Port() {
  {
    Serial.println("*** Testing HardwareScanner with existing RF and GPS port on feather....");
    CansatHW_Scanner hw;
    hw.init(4, 10, 100, 1, 2);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
  }
  Serial.println("");

  {

    Serial.println("*** Testing HardwareScanner with unexistant RF and GPS port....");
    CansatHW_Scanner  hw;
    hw.init(4, 10, 100, 5, 6);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
  }
  Serial.println("");
}


void testSubclass() {
  Serial.println("*** Testing HardwareScanner subclass with 3 EEPROMS, 32k+16k+8k and SD Card on CS 10....");
#ifdef USING_SIMULATED_ARDUINO
  Serial.println("Second EEPROM chip should be reported as out-of-order");
#endif
  HW_Subclass hw;
  hw.init(10,10, 100);
  Serial.println("Full diagnostic:");
  hw.printFullDiagnostic(Serial);
  checkAvailabilityOfSerialPorts(hw);
  Serial.println("");
}

void testHardwareScanner() {

  Serial.println("*** Testing CansatHW_Scanner class with real hardware....");
  {
    Serial << F("*** Testing HardwareScanner with existing RF port 1 (inexistent on UNO, ok on Feather, TMinus, Mega..., GPS port 2") << ENDL;
    CansatHW_Scanner hw;
    hw.init(4, 1, 127, 1, 2);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
#ifdef RF_ACTIVATE_API_MODE
    Serial << ENDL << F("RF port: ") << (hw.getRF_XBee() != NULL ? "OK" : "NOK") << ENDL;
#else
    Serial << ENDL << F("RF port: ") << (hw.getRF_SerialObject() != NULL ? "OK" : "NOK") << ENDL;
#endif
    Serial << ENDL;
  }
  {
    Serial << F("*** Testing HardwareScanner with RF port 2 (inexistent on UNO, ok on Feather, TMinus, Mega..., no GPS") << ENDL;
    CansatHW_Scanner hw;
    hw.init(4, 1, 127, 2,0);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
 #ifdef RF_ACTIVATE_API_MODE
    Serial << ENDL << F("RF port: ") << (hw.getRF_XBee() != NULL ? "OK" : "NOK") << ENDL;
#else
    Serial << ENDL << F("RF port: ") << (hw.getRF_SerialObject() != NULL ? "OK" : "NOK") << ENDL;
#endif
  Serial << ENDL;
  }

  {
    Serial << F("*** Testing HardwareScanner with RF port 3 (inexistent on UNO and Feather, ok on TMinus, Mega...), no GPS") << ENDL;
    CansatHW_Scanner hw;
    hw.init(4, 1, 127, 3,0);
    Serial.println("Full diagnostic:");
    hw.printFullDiagnostic(Serial);
    checkAvailabilityOfSerialPorts(hw);
#ifdef RF_ACTIVATE_API_MODE
    Serial << ENDL << F("RF port: ") << (hw.getRF_XBee() != NULL ? "OK" : "NOK") << ENDL;
#else
    Serial << ENDL << F("RF port: ") << (hw.getRF_SerialObject() != NULL ? "OK" : "NOK") << ENDL;
#endif
  Serial << ENDL;
  }
}
//************************************************************

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
#ifdef USING_SIMULATED_ARDUINO

  Serial.println("Testing with simulated Arduino environment");

  testHW_ScannerWithoutRF_Port();
  testHW_ScannerWithRF_Port();
#else
  testHardwareScanner();
#endif
  testSubclass();
  Serial << F("Size of HardwareScanner class: ") << sizeof(HardwareScanner) << F(" bytes") << ENDL;
  Serial.println("*** End of test ***");

}

void loop() {
  // put your main code here, to run repeatedly:

}
