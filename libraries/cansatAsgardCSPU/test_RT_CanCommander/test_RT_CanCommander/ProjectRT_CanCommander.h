/*
 * ProjectRT_CanCommander.h
 */

#pragma once
#include "RT_CanCommander.h"
#include "CansatXBeeClient.h"

enum ProjectCmdRequest {
	aProjectCommand=100
};

class ProjectRT_CanCommander : public RT_CanCommander {
public:
	ProjectRT_CanCommander(unsigned long int theTimeOut) : RT_CanCommander(theTimeOut) {};
protected:
	virtual bool processProjectCommand(int requestType, char* cmd) {
		Serial << "ProjectRT_CanCommander::processProjectCommand: type = "<< requestType  << ENDL;
		if (requestType == (int) ProjectCmdRequest::aProjectCommand) {
			RF_OPEN_CMD_RESPONSE(RF_Stream);
			*RF_Stream << "101, response args here";
			RF_CLOSE_CMD_RESPONSE(RF_Stream);
			return true;
		} else return false;
	};
};
