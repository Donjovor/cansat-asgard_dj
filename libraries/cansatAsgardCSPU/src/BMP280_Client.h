/*
    BMP280_Client.h
*/
#pragma once
#include "CansatConfig.h"
#ifdef BMP_USE_BMP3XX_MODEL 
#  include "Adafruit_BMP3XX.h" 
#else
#  include "Adafruit_BMP280.h"
#  include "Adafruit_Sensor.h"
#endif

#include "Wire.h"
#include "CansatRecord.h"

/** @ingroup cansatAsgardCSPU
    @brief Interface class to the BMP280 sensor (connected to the I2C bus),
    providing the required methods to access individual
    data and to fill the relevant fields of a CanSat record.
*/
class BMP280_Client {
  public:
    BMP280_Client();
    /**
        @brief Initialize the BMP280 sensor before use.
        @param theSeaLevelPressure The pressure at sea level in hPa (required
        	   to compute altitude).
        @return True if initialization was successful, false otherwise.
    */
    bool begin(float theSeaLevelPressure);

    /** Obtain the atmospheric pressure in hPa
     *  @param numSamples The number of sensor readings to average.
     *  @return The current pressure, or 0 if reading failed */
    float getPressure(uint8_t numSamples=BMP_NumSamplesForPressureReading);

    /** Obtain the altitude (from sea level) in m as computed using the current pressure and the
     *  sea level pressure provide during initialization.
     *  It is obtained from a _single_ pressure reading. For a better
     *  stability, read pressure first with getPressure(), using the appropriate number
     *  of samples and derive altitude afterwards using convertPressureToAltitude();
     *  @return The current altitude. 0 if the calculation fails for any reason.*/
    float getAltitude();

    /** Obtain the atmospheric temperature in °C
     *  @return The current temperature.*/
    float getTemperature() ;

    /** Convert a pressure value in altitude (from sea level) in m as computed using the current pressure and the
     *  sea level pressure provide during initialization.
     *  @param pressureInHPa The pressure to convert.
     *  @return The current altitude. 0 if the calculation fails for any reason.*/
    float convertPressureToAltitude(float pressureInHPa);

    /** @brief Read sensor data and populate data record.
	 *  @param record The data record to populate (fields BMPtemperature, altitude and pressure).
	 *  @return True if reading was successful.
	 */
     bool readData(CansatRecord& record);

  protected:
#ifdef BMP_USE_BMP3XX_MODEL 
    Adafruit_BMP3XX bmp;  //**< The driver class.
#else
    Adafruit_BMP280 bmp;  //**< The driver class.
#endif

    float seaLevelPressure; //**< Atmospheric pressure at sea level, in hPA
#ifdef INCLUDE_DESCENT_VELOCITY
    float previousAltitude=-1000; /**< Previously measured altitude, for velocity calculation.
    								   -1000 denotes "no previous altitude" */
    uint32_t previousTimestamp;   /**< timestamp of previous altitude */
#endif
};
