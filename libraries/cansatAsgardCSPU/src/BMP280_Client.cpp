/*
   BMP280_Client.cpp
*/
#include "BMP280_Client.h"
#define DBG_VELOCITY 0
#define DBG_BMP_TIMING 0
#define DBG_BMP_INIT 1
#define DBG_DIAGNOSTIC 1

BMP280_Client::BMP280_Client(): bmp(), seaLevelPressure(0.0) {}

bool BMP280_Client::begin(float theSeaLevelPressureInHPa) {
#ifdef BMP_USE_BMP3XX_MODEL
  DPRINTSLN(DBG_BMP_INIT, "INITIALISATING BMP3XX");
  if (!bmp.begin_I2C()) return false;
  bmp.setTemperatureOversampling(BMP3_OVERSAMPLING_8X); //(defaults to BMP3_NO_OVERSAMPLING)
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_16X); //(defaults to BMP3_NO_OVERSAMPLING)
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_DISABLE);
  //(defaults to BMP3_IIR_FILTER_DISABLE) Do not use IIR filter to avoid degrading the response time.
  bmp.setOutputDataRate(BMP3_ODR_50_HZ);
  //(defaults to BMP3_ODR_25_HZ)
#else
  DPRINTSLN(DBG_BMP_INIT, "INITIALISATING BMP280");
  if (!bmp.begin()) return false;
#endif

  seaLevelPressure = theSeaLevelPressureInHPa;
  DPRINTS(DBG_BMP_INIT, "BMP: Sea-level pressure (hPa)=");
  DPRINTLN(DBG_BMP_INIT, theSeaLevelPressureInHPa);
  DPRINTS(DBG_BMP_INIT, "     Samples for pressure reading=");
  DPRINTLN(DBG_BMP_INIT, BMP_NumSamplesForPressureReading);
  DPRINTS(DBG_BMP_INIT, "     Periods for desc. velocity calculation:");
  DPRINTLN(DBG_BMP_INIT, BMP_NumPeriodsForDescentVelocityCalculation);
  DPRINTS(DBG_BMP_INIT, "     Desc. velocity samples averaged:");
  DPRINTLN(DBG_BMP_INIT, BMP_NumDescentVelocitySamplesToAverage);
  return true;
}

float BMP280_Client::getPressure(uint8_t numSamples) {
  float result = 0;
  for (int i = 0; i < numSamples; i++) {
#ifdef BMP_USE_BMP3XX_MODEL
    if (! bmp.performReading()) {
      DPRINTSLN(DBG_DIAGNOSTIC, "BMP failed to perform reading");
      return 0;
    }
    else {
      result += bmp.pressure;
    }
#else
    result += bmp.readPressure();
#endif
  }
  result /= 100.0;
  result /= numSamples;
  return result;
}

float BMP280_Client::convertPressureToAltitude(float pressureInHPa)
{
  float altitude = 44330 * (1.0 - pow(pressureInHPa / seaLevelPressure, 0.1903));
  if (isnan(altitude)) {
    altitude = 0.0;
  }
  return altitude;
}

float BMP280_Client::getAltitude() {
  return convertPressureToAltitude(getPressure());
}

float BMP280_Client::getTemperature() {
#ifdef BMP_USE_BMP3XX_MODEL
  if (! bmp.performReading()) {
    DPRINTSLN(DBG_DIAGNOSTIC, "BMP failed to perform reading");
    return 0;
  }
  else {
    return bmp.temperature;
  }
#else
  return bmp.readTemperature();
#endif
}

bool BMP280_Client::readData(CansatRecord& record) {

#if DBG_BMP_TIMING == 1
  uint32_t tsStart = millis();
#endif
  record.pressure = getPressure();
#if DBG_BMP_TIMING == 1
  uint32_t tsPressure = millis();
#endif
  record.altitude = convertPressureToAltitude(record.pressure);
#if DBG_BMP_TIMING == 1
  uint32_t tsAltitude = millis();
#endif
#ifdef BMP_USE_BMP3XX_MODEL
  record.temperatureBMP = bmp.temperature; // performReading() was already called by getPressure()
#else
  record.temperatureBMP = getTemperature();
#endif

#if DBG_BMP_TIMING == 1
  uint32_t tsTemperature = millis();
  Serial << "Reading durations (msec): pressure=" << tsPressure - tsStart
         << ", altitude=" << tsAltitude - tsPressure
         << ", temperature=" << tsTemperature - tsAltitude << ENDL;
#endif

#ifdef INCLUDE_DESCENT_VELOCITY
  float now = millis();
  DPRINTS(DBG_VELOCITY, "now= ");
  DPRINT(DBG_VELOCITY, now);
  DPRINTS(DBG_VELOCITY, ", previousTS= ");
  DPRINTLN(DBG_VELOCITY, previousTimestamp);
  DPRINTS(DBG_VELOCITY, "altitude= ");
  DPRINT(DBG_VELOCITY, record.altitude, 5);
  DPRINTS(DBG_VELOCITY, ", previous=") ;
  DPRINTLN(DBG_VELOCITY, previousAltitude, 5);

  if (previousAltitude > -1000) {
    record.descentVelocity = // NB: positive towards the ground.
      (previousAltitude - record.altitude) / (now - previousTimestamp);
    record.descentVelocity *= 1000.0; // Convert from m/msec into m/s
  } else {
    record.descentVelocity = -1000;
  }
  previousTimestamp = record.timestamp;
  previousAltitude = record.altitude;
#endif
  return true;
}
