/*
   ThermistorNTCLE101E3103SB0.h
*/

#pragma  once
#include "ThermistorSteinhartHart.h"

/** @ingroup cansatAsgardCSPU
  @brief This a subclass of ThermistorSteinhartHart customized for thermistor NTCLE101E3103SB0.
  Use this class to read thermistor resistance and convert to degrees.
  Wiring: VCC to thermistor, thermistor to serialresistor, serialresistor to ground.
*/

class ThermistorNTCLE101E3103SB0: public ThermistorSteinhartHart {
  public:
    /**
      @param theVcc  The voltage supplied to the serial resistor+thermistor assembly
      @param theAnalogPinNbr The pin connected to the thermistor-to-resistor connexion.
      @param theSerialResistor The value of the resistor connected to the thermistor (ohms).
    */
    ThermistorNTCLE101E3103SB0(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorSteinhartHart(theVcc, theAnalogPinNbr, 10000, 0.003354016,  0.0002569850, 2.620131E-06, 6.383091E-08, theSerialResistor) {};
};
