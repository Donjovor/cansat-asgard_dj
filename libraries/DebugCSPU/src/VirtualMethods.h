/*  @file
 *  The VIRTUAL/VIRTUAL_FOR_TEST/NON_VIRTUAL macros allows for forcing 
 *  additional methods to be virtual in simulated test environments where 
 *  it could be required to pverride methods that do not require overriding
 *  when running on Arduino hardware.
 *  
 *  When building in such simulated environment, USE_TEST_VIRTUAL_METHODS will
 *  be defined. 
 */
#ifdef USE_TEST_VIRTUAL_METHODS
#  define NON_VIRTUAL 
#  define VIRTUAL_FOR_TEST virtual
#  define VIRTUAL virtual
#else
/** @ingroup DebugCSPU
 *  @{
 *  @brief Use this macro where the word "virtual" would be legal to identify a method that should _never_ be virtual
 *  
 *  This macro will always be defined as blank, whatever the value of USE_TEST_VIRTUAL_METHODS
 */
#  define NON_VIRTUAL
/**  @brief Use this macro where the word "virtual" would be legal to identify a method that should be virtual
 *   during some tests, and not in the final software.
 *  
 *  This macro will be defined as 'virtual', if USE_TEST_VIRTUAL_METHODS if defined, and as blank otherwise.
 */
#  define VIRTUAL_FOR_TEST
/**  @brief Use this macro where the word "virtual" would be legal to identify a method that should _always_ be virtual
 *  
 *  This macro will always be defined as 'virtual', whatever the value of USE_TEST_VIRTUAL_METHODS
 */
#  define VIRTUAL virtual  
#endif 
