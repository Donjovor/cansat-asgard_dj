/*
 * IsaTwoHardwareScanner
 * 
 * This utility just creates an IsaTwoHardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */

#include "IsaTwoHW_Scanner.h"  

 void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.println("Running IsaTwo Hardware Scanner...");
  Serial.println("Note: be sure to have pull-ups on SCL-SDA, otherwise detection is not reliable on Feather M0 Express");
  Serial.flush();
  IsaTwoHW_Scanner hw;
  hw.IsaTwoInit();
  hw.printFullDiagnostic(Serial);
}

void loop() {
  digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
  delay(500);
}
