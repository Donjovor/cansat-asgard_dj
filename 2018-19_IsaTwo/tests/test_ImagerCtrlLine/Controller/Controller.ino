
constexpr byte CtrlLine = 6;

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial.print("Writing Imager CtrlLine =");
  Serial.println(CtrlLine);
  pinMode(CtrlLine, OUTPUT);
}

void loop() {
  digitalWrite(CtrlLine, !digitalRead(CtrlLine));
  Serial.print("  now ");
  Serial.println( (digitalRead(CtrlLine)==LOW ? "LOW" : "HIGH"));
  delay(5000);

}
