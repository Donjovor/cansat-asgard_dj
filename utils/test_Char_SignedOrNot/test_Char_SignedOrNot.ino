/*
 * Test program to check the default signed-ness of char on the various boards.
 * 
 * On Uno, char is a signed char, by default. Result of the program:
 *    Char value is -1
 *    Unsigned char value is 255
 *    Signed char value is -1
 * On Feather M0 Express, char is unsigned by default. Result of the program:
 *    Char value is 255
 *    Unsigned char value is 255
 *    Signed char value is -1
 */
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial) delay(10);
  char c = -1;
  Serial.print("Char value is ");
  Serial.println( (int) c);

  unsigned char uc = -1;
  Serial.print("Unsigned char value is ");
  Serial.println( (int) uc);

  signed char sc = -1;
  Serial.print("Signed char value is ");
  Serial.println( (int) sc);
}

void loop() {
  // put your main code here, to run repeatedly:

}
