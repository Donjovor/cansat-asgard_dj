
const unsigned long baudRate=115200;

/*
 * This sketch demonstrates un problem with TMinus µC  (or ansevere misunderstanding on our side)
 * Try inputing 2 ou 3 digits integers on the serial interface using Arduino IDE:
 *    On TMinus, it works with any baud rate <= 74880, but when used with baudrate=115200, 
 *    it fails: only the first digit of any multidigit integer is shown.
 *    
 *    On Genuino Uno, it works perfectly at any baudrate....
 */

void setup() {
  // put your setup code here, to run once:
  Serial.begin(baudRate);
  while(!Serial) ;
}

void loop() {
  int n=0;
  while (Serial.available()) Serial.read();
  Serial.print(F("Enter a positive integer: "));
  while (n == 0 )  n = Serial.parseInt();
  Serial.println(n);
  delay(1000);
}
