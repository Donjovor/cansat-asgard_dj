// Wire Master Reader
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Reads data from an I2C/TWI slave device
// Refer to the "Wire Slave Sender" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>

void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  Wire.onRequest(requestEvent); // register event
  Serial.begin(115200);  // start serial for output
  while (!Serial) ;
  Serial.println("I2C 2-way communication : board 1 ");
}

int counter=0;

void loop() {
  counter++;
  Wire.requestFrom(8, 6);    // request 6 bytes from slave device #8

  while (Wire.available()) { // slave may send less than requested
    char c = Wire.read(); // receive a byte as character
    Serial.print(c);         // print the character
  }
  if ((counter %20)==0) Serial.println();
  delay(500);
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Wire.write("12345 "); // respond with message of 6 bytes
  // as expected by master
}
