
   /** @name Protected methods to be overridden by projects-specific subclasses.
     *  @{   */
 
 	/** @} */  // End of group of methods to be overridden.
 